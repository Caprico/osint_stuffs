import os
import json
import requests

def save_json_file(ip, json_response):
    print(json_response)
    json_file_name = "{0}.json".format(ip)

    with open('{0}'.format(json_file_name), 'a') as json_file:
        json_file.write("{0}".format(json.dumps(json_response)))
        print('Wrote {0} file'.format(ip))

def get_shodan(ips, api_key):
    responses_dict = {}
    for ip in ips:
        json_response = requests.get("https://api.shodan.io/shodan/host/{0}?key={1}".format(ip, api_key))

        responses_dict[ip] = json_response

        save_json_file(ip, json_response.json())
    return responses_dict


def get_ip_list(path):
    with open(path) as f:
        contents = f.readlines()
    contents = [ip.strip() for ip in contents]

    return contents

if __name__ == "__main__":
    file_path = input("file_name: ")
    api_key = input("api_key: ")

    ips = get_ip_list(file_path)

    shodan_responses = get_shodan(ips, api_key)



    for ip,json in shodan_responses.items():
        print("{0}:{1}".format(ip,json))
