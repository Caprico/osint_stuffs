#!/usr/bin/env python
import requests
from xml.etree import cElementTree as ET

def getList(url):
    r = requests.get(url,timeout=5)
    return r.text

def parseXML(source):
    root = ET.fromstring(source)
    for block in list(root):
        ip = block.find('ipv4').text
        print(ip)
        
parseXML(getList("https://isc.sans.edu/api/threatlist/shodan"))
